<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $fillable = [
        'name', 'email','message','is_deleted',
    ];
    protected $table = 'contactus';
}
