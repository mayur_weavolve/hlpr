<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankDetail extends Model
{
    protected $fillable = [
        'user_id', 'account_title','account_number', 'bank_name','iban_number','doc_image','aboutme','user_type',
    ];
}
