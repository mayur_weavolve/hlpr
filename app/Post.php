<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'user_id', 'expert_id','question','anonymusly','image','description','video','user_type','address','latitude','longitude',
    ];
    public function answers()
    {
        return $this->hasMany('App\PostAnswer');
    }
    public function expert()
    {
        return $this->hasMany('App\Expert','id','expert_id');
    }
    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }
    public function post_like()
    {
        return $this->hasMany('App\PostLike');
    }
    public function post_comment()
    {
        return $this->hasMany('App\PostComment');
    }
    public function post_bookmark()
    {
        return $this->hasMany('App\PostBookmark');
    }
    public function post_notification()
    {
        return $this->hasMany('App\Notification');
    }
   
}
