<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'post_id', 'user_id','notifier_id','notification','status','type'
    ];
    public function post()
    {
      return $this->belongsTo(Post::class);
    }
    public function user()
    {
        return $this->hasOne('App\User','id','notifier_id');
    }
}
