<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardDetail extends Model
{
    protected $fillable = [
        'user_id', 'card_number','card_holder_name', 'exp_month','exp_year','cvc','document_image'
    ];
}
