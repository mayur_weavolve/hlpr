<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserExperties extends Model
{
    protected $fillable = [
        'user_id', 'expert_id',
    ];
    public function rating()
    {
        return $this->hasMany('App\Rating');
    }
    public function expert()
    {
        return $this->hasMany('App\Expert','id','expert_id');
    }
    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }
    
}
