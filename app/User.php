<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;


class User extends Authenticatable
{
    use Notifiable,HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username','password','location','designation','interests','expertise',
        'mobileno','role_id','provider','provider_id','image','role_type','cover_image','about_me','status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function rating()
    {
        return $this->hasMany("App\Rating","experties_id");
    }
    public function userExpertise()
    {
        return $this->hasMany("App\UserExperties","user_id");
    }
    public function post_bookmark()
    {
        return $this->hasMany('App\PostBookmark');
    }
    
}
