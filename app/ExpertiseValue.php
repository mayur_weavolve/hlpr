<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpertiseValue extends Model
{
    protected $fillable = [
        'user_id', 'transation_id','price',
    ];
}
