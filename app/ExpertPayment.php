<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpertPayment extends Model
{
    protected $fillable = [
        'user_id', 'transation_id','payment_status','payment_amount','payment_date',
    ];
}
