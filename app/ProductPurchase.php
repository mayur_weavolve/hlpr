<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPurchase extends Model
{
    protected $fillable = [
        'user_id','transaction_id','sku','price','platform',
    ];
}
