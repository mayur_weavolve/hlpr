<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostAnswer extends Model
{
    protected $fillable = [
        'user_id', 'post_id','experties_id','answer'
    ];
    public function post()
    {
      //return $this->hasMany('App\Post','post_id');
      return $this->belongsTo(Post::class);
    }
}
