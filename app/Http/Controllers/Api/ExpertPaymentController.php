<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Validator;
use App\ExpertPayment;

class ExpertPaymentController extends Controller
{
    public function add_payment(Request $request)
    {
        $user = Auth::user();
        $data['user_id'] = $user->id;
        $data['payment_stauts'] = 'pending';
        $add = ExpertPayment::create($data);

        return response()->json(['statusCode' => '200','data' =>$add ,"message" =>"Payment add Sucessfully......"]);
    }
}
