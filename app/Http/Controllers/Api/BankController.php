<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Validator;
use App\BankDetail;

class BankController extends Controller
{
    public function add_bank_detail(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($request->all(), [ 
            'account_title' => 'required', 
            'account_number' => 'required', 
            'bank_name' => 'required', 
            'iban_number' => 'required',
            'doc_image' => 'required',
            'aboutme' => 'required',
        ]);
        if ($validator->fails()) { 
            return response()->json(['statusCode' => '400','data' => $validator->errors() ,"message" =>"Somthing Wrong Please try again"]);
        }
        $user = Auth::user();
        $bank = BankDetail::where('user_id',$user->id)->get()->first();
        if($bank)
        {
            $bank->account_title = $request->input('account_title');
            $bank->account_number = $request->input('account_number');
            $bank->bank_name = $request->input('bank_name');
            $bank->iban_number = $request->input('iban_number');
            $bank->doc_image = $request->input('doc_image');
            $bank->aboutme = $request->input('aboutme');
            $bank->user_type = $request->input('user_type');
            if ($request->file('doc_image')!=null){    
                $bank->doc_image = $request->file('doc_image')->hashName();
                $request->file('doc_image')
                ->store('post', ['disk' => 'public']);
                $bank->save();  
                
                $user->about_me = $bank->aboutme;
                $user->role_type = $bank->user_type;
                // $user->role_id = "3";
                $user->save();
            }   
        }
        else
        {
            if ($request->file('doc_image')!=null){    
                $data['doc_image'] = $request->file('doc_image')->hashName();
                $request->file('doc_image')
                ->store('post', ['disk' => 'public']);    
            }
            $data['user_id'] = $user->id;
            $bank = BankDetail::create($data);

            $user->about_me = $bank->aboutme;
            $user->save(); 
        }
        return response()->json(['statusCode' => '200','data' =>$bank ,"message" =>"Bank details have save successfully....!"]);
    }
    public function get_bank_detail()
    {
        $user = Auth::user();
        $bank = BankDetail::where('user_id',$user->id)->get()->first();
        if($bank)
        {
            return response()->json(['statusCode' => '200','data' =>$bank ,"message" =>"Bank details....!"]);
        }
        else{
            // $response['']
            return response()->json(['statusCode' => '400','data' =>null ,"message" =>"No data found....!"]);
        }
        
    }
}
