<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Validator;
use App\ProductPurchase;
//use for order creation
class ProductPurchaseController extends Controller
{
    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'transaction_id' => 'required',
            'price' => 'required',
            'sku' => 'required',
            'platform' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['statusCode' => '400','data' => $validator->errors() ,"message" =>"Somthing Wrong Please try again"]);
        }
        $data = $request->all();
        $user = Auth::user();
        $data['user_id'] = $user->id;
        $add = ProductPurchase::create($data);

        $user->points = $user->points + $add->price;
        $user->save();

        return response()->json(['statusCode' => '200','data' =>$add ,"message" =>"Order create sucessfully....!"]);
    }
}
