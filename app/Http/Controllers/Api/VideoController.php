<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Config;
use Validator;

class VideoController extends Controller
{
    public function video_upload(Request $request)
    {
        $data = $request->all();
        $data['video'] = $request->file('video');
        $dir = $data['dir'];
        if ($request->file('video')==null){        
        }
        else{
            $data['video'] = $request->file('video')->hashName();
            $request->file('video')
            ->store($dir, ['disk' => 'public']);
            $data['url'] = Config::get('constants.video').$dir.'/'.$data['video'];        
        }
        return response()->json(['statusCode' => '200','data' =>  $data,"message" =>"video upload Successfully....!"]);
    }
}
