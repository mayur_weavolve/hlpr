<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\ContactUs;

class ContactUsController extends Controller
{
    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'message' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['statusCode' => '400','data' => $validator->errors() ,"message" =>"Somthing Wrong Please try again"]);
        }
        $data = $request->all();
        $add = ContactUs::create($data);

        return response()->json(['statusCode' => '200','data' =>$add ,"message" =>"Thank you for giving your feedback....!"]);
    }
}
