<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Validator;
use App\User;
use App\Post;
use App\PostExperties;
use App\PostLike;
use App\PostComment;
use App\PostAnswer;
use App\PostBookmark;
use App\Notification;

class PostController extends Controller
{
    public function post_add(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            // 'expert_id' => 'required',
            'question' => 'required',
            
        ]);
        if ($validator->fails()) {
            return response()->json(['statusCode' => '400','data' => $validator->errors() ,"message" =>"Somthing Wrong Please try again"]);
        }
        $data = $request->all();
        $data['user_id'] = $user->id;
        $data['is_deleted'] = "0";
        // if ($request->file('image')!=null){    
        //     $data['image'] = $request->file('image')->hashName();
        //     $request->file('image')
        //     ->store('post', ['disk' => 'public']);    
        // }
        $post_add = Post::create($data);
        if(($data['expert_id'] == null) && ($data['expert'] == null)){
            return response()->json(['statusCode' => '400','data' =>null ,"message" =>"Please select expert or category"]);
        }
        if($request->get('expert')) 
        {
            foreach($data['expert'] as $expert){
                if($expert){
                    $item = [];
                    $item['question_id'] = $post_add->id;
                    $item['experties_id'] = $expert['experties_id'];
                    $experties = PostExperties::create($item);

                    $data['post_id'] = $post_add->id;
                    $data['user_id'] = $expert['experties_id'];
                    $data['notifier_id'] = $user->id;
                    $data['notification'] = $user->username.' Ask question for you ' ;
                    $data['type'] = 'add';
                    $notification = Notification::create($data);
                }
            }
            if($request->get('anonymusly'))
            {
                $post_add->anonymusly = '1';
                $post_add->save();
            }
            else{
                $post_add->anonymusly = '0';
                $post_add->save();
            }
        $post_add->expert = '1';
        $post_add->save();
        }
        else{
            $post_add->expert = '0';
            $post_add->save();
        }
        $data['post_id'] = $post_add->id;
        $data['user_id'] = $post_add->user_id;
        $data['notifier_id'] = $user->id;
        $data['notification'] = $user->username.' Add Post';
        $data['type'] = 'add';
        $notification = Notification::create($data);
        
        
        return response()->json(['statusCode' => '200','data' =>$post_add ,"message" =>"Your question will soon be answered by a hlpr"]);
    }
    public function post_like(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $post = PostLike::where('user_id',$user->id )->where('post_id',$data['post_id'])->get()->first();
        $post_list = Post::where('id',$data['post_id'])->get()->first();
        $post_notification = Notification::where('post_id',$data['post_id'])
                                ->where('type','like')->where('notifier_id',$user->id)->get()->first();   
        // print_r($post_notification);
        // exit;              
        if($post)
        {
            $post->delete();
            $post_notification->delete();
            return response()->json(['statusCode' => '200',"message" =>"Post Unlike....!"]);
        }
        else{
            $data['user_id'] = $user->id;
            $post_like = PostLike::create($data);
        }
        $data['post_id'] = $post_like->post_id;
        $data['user_id'] = $post_list->user_id;
        $data['notifier_id'] = $user->id;
        $data['notification'] = $user->username.' like your post';
        $data['type'] = 'like';
        $notification = Notification::create($data);
        return response()->json(['statusCode' => '200','data' =>$post_like ,"message" =>"Posts Like....!"]);
    
    }
    public function post_comment(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $post_list = Post::where('id',$data['post_id'])->get()->first();
        $validator = Validator::make($request->all(), [
            'comment' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['statusCode' => '400','data' => $validator->errors() ,"message" =>"Somthing Wrong Please try again"]);
        }
       
        $data['user_id'] = $user->id;
        $post_comment = PostComment::create($data);
       
        $data['post_id'] = $post_comment->post_id;
        $data['user_id'] = $post_list->user_id;
        $data['notifier_id'] = $user->id;
        $data['type'] = 'comment';
        $data['notification'] = $user->username.' responded to your post';
        $notification = Notification::create($data);

        return response()->json(['statusCode' => '200','data' =>$post_comment ,"message" =>"Thank you for your response"]);
    }
    public function post_answer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'experties_id' => 'required',
            'post_id' => 'required',
            'answer' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['statusCode' => '400','data' => $validator->errors() ,"message" =>"Somthing Wrong Please try again"]);
        }
        $user = Auth::user();
        $data = $request->all();
        $data['user_id'] = $user->id;
        $post_answer = PostAnswer::create($data);

        $data['post_id'] = $post_answer->post_id;
        $data['user_id'] = $post_answer->user_id;
        $data['notifier_id'] = $user->id;
        $data['type'] = 'comment';
        $data['notification'] = $user->username.' responded to your post';
        $notification = Notification::create($data);

        return response()->json(['statusCode' => '200','data' =>$post_answer ,"message" =>"Post Answer Add successfully....!"]);
    }
    public function post_list(Request $request)
    {
        $user = Auth::user();
        $search = $request->get('search');
        $cat_id = $request->get('cat_id');
        $type = $request->get('user_type');
        $user_id = $request->get('user_id');
        $where = [];
        $where[] = ['posts.question','like','%'.$search.'%'];
        $where[] = ['posts.user_type','like','%'.$type.'%'];

        if($cat_id)
        {
            $where[] = ['posts.expert_id','=',$cat_id];
        }
        if($user_id)
        {
            if($user_id == "1"){
                $where[] = ['posts.user_id','=',  $user->id];
            }else{
                $where[] = ['posts.user_id','=',  $user_id];
            }
        }
        $post_list = Post::select('posts.expert as for_expert', 'posts.*')->with('expert','user')
        //->join('experts','experts.id','posts.expert_id')
        ->join('users','users.id','posts.user_id')
        ->where($where)
        ->orderBy("posts.id","desc")
        ->paginate(10);
        foreach($post_list as &$post){
            $post['answerCount'] = PostComment::where('post_id',$post->id)->count();
            $post['likeCount'] = PostLike::where('post_id',$post->id)->count();
            $post['bookmarkCount'] = PostBookmark::where('post_id',$post->id)->count();
            $post['is_like'] = (boolean) PostLike::where('post_id',$post->id)->where('user_id',$user->id)->count();
            $post['is_bookmark'] = (boolean) PostBookmark::where('post_id',$post->id)->where('user_id',$user->id)->count();
            if($post->for_expert == 1){
                 $post['canComment'] =  PostExperties::where("question_id", $post->id)->where("experties_id", $user->id)->limit(1)->count();
            }else{
                $post['canComment'] = 1;
            }
        }
        if(count($post_list) > 0){
            return response()->json(['statusCode' => '200','data' =>$post_list ,"message" =>"Post Answer List....!"]);
        }
        else{
            return response()->json(['statusCode' => '402','data' =>NULL ,"message" =>"No Record Found....!"]);
        }
    }
    public function post_like_list($id)
    {
        $user = Auth::user();
        $post = Post::find($id);
        $post_like =  PostLike::with('user:id,name,image')->where('post_id',$post->id)->get();
        return response()->json(['statusCode' => '200','data' =>$post_like ,"message" =>"Post Like List....!"]);
        
    }
    public function post_comment_list($id)
    {
        $user = Auth::user();
        $post = Post::find($id);
        $post_comment = PostComment::with('user:id,name,image')->where('post_id',$post->id)->get();
        return response()->json(['statusCode' => '200','data' =>$post_comment ,"message" =>"Post Comment List....!"]);
        
    }
    public function post_bookmark(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $post_bookmark = PostBookmark::where('user_id',$user->id )->where('post_id',$data['post_id'])->get()->first();
        if($post_bookmark)
        {
            $post_bookmark->delete();
            return response()->json(['statusCode' => '200',"message" =>"Removed from Bookmarks....!"]);
        }
        else{
            $data['user_id'] = $user->id;
            $post_bookmark = PostBookmark::create($data);
            return response()->json(['statusCode' => '200','data' =>$post_bookmark,"message" =>"Added to Bookmarks....!"]);
        }
    }
    public function post_bookmark_list()
    {
        $user = Auth::user();
        $post_bookmark = PostBookmark::with('post','user')
            ->where('post_bookmarks.user_id', $user->id)
            ->select('id','post_id','user_id')
            ->orderBy("id","desc")
            ->paginate(10);
        foreach($post_bookmark as &$post){
            $post['answerCount'] = PostAnswer::where('post_id',$post->id)->count();
            $post['likeCount'] = PostLike::where('post_id',$post->id)->count();
            $post['bookmarkCount'] = PostBookmark::where('post_id',$post->id)->count();
            $post['is_like'] = (boolean) PostLike::where('post_id',$post->id)->where('user_id',$user->id)->count();
            $post['is_bookmark'] = (boolean) PostBookmark::where('post_id',$post->id)->where('user_id',$user->id)->count();
        }
        if(count($post_bookmark) > 0){
            return response()->json(['statusCode' => '200','data' =>$post_bookmark ,"message" =>"Post Bookmark List....!"]);
        }
        else{
            return response()->json(['statusCode' => '200','data' =>[] ,"message" =>"No Record Found....!"]);
        }
    }
    public function notification()
    {
        $user = Auth::user();
        $notification = Notification::with('user')->where('user_id',$user->id)->orderBy("created_at","desc")->get();
        return response()->json(['statusCode' => '200','data' =>$notification ,"message" =>"Notification list....!"]);
        
    }
    public function post_delete($id)
    {
        $user = Auth::user();
        $post_delete = Post::find($id);
        $post_delete->delete();
        return response()->json(['statusCode' => '200','data' =>null ,"message" =>"post deleted successfully....!"]);
    }
    public function post_edit($id,Request $request)
    {
        $user = Auth::user();
        
        $data = $request->all();
        $post_edit = Post::find($id);

        $post_edit->user_id= $user->id;
        $post_edit->question = $request->input('question');
        $post_edit->description = $request->input('description');
        $post_edit->anonymusly = $request->input('anonymusly');
        $post_edit->expert_id = $request->input('expert_id');
        $post_edit->expert = $request->input('expert');
        $post_edit->user_type = $request->input('user_type');
        $post_edit->address  = $request->input('address ');
        $post_edit->longitude = $request->input('longitude');
        $post_edit->latitude  = $request->input('latitude ');
        $post_edit->image = $request->input('image');
        $post_edit->video = $request->input('video');
        $post_edit->save();

        return response()->json(['statusCode' => '200','data' =>$post_edit ,"message" =>"post updated successfully....!"]);
    }

    public function post_detail($id)
    {
        $user = Auth::user();
        $post_list = Post::select('posts.expert as for_expert', 'posts.*')->with('expert','user')
        ->join('users','users.id','posts.user_id')
        ->where('posts.id',$id)
        ->get();
        foreach($post_list as &$post){
            $post['answerCount'] = PostComment::where('post_id',$post->id)->count();
            $post['likeCount'] = PostLike::where('post_id',$post->id)->count();
            $post['bookmarkCount'] = PostBookmark::where('post_id',$post->id)->count();
            $post['is_like'] = (boolean) PostLike::where('post_id',$post->id)->where('user_id',$user->id)->count();
            $post['is_bookmark'] = (boolean) PostBookmark::where('post_id',$post->id)->where('user_id',$user->id)->count();
            if($post->for_expert == 1){
                 $post['canComment'] =  PostExperties::where("question_id", $post->id)->where("experties_id", $user->id)->limit(1)->count();
            }else{
                $post['canComment'] = 1;
            }
        }
        if(count($post_list) > 0){
            return response()->json(['statusCode' => '200','data' =>$post_list ,"message" =>"Post Answer List....!"]);
        }
        else{
            return response()->json(['statusCode' => '402','data' =>NULL ,"message" =>"No Record Found....!"]);
        }
    }
}
