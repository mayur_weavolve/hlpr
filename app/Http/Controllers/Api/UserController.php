<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\UserExperties;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Hash;
use Validator;
use Config;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email|unique:users', 
            'password' => 'required', 
            'username' => 'required',
            'location' => 'required',
            'mobileno' => 'required', 
            'designation' => 'required',
            // 'interests' => 'required',
        ]);
        if ($validator->fails()) { 
            return response()->json(['statusCode' => '400','data' => $validator->errors() ,"message" =>"Somthing Wrong Please try again"]);
        }
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        $data['interests'] = 'null';
        $user = User::create($data);

        if($user->role_type == "user")
        {
            $user->role_id = '2';
            $user->status = 'active';
            $user->save();
        }
        else{
            $user->role_id = '3';
            $user->status = 'inactive';
            $user->save();
        }

        $data['user_id'] = $user->id;
        // $data['expert_id'] = $data['expert_id'];
        $experties = UserExperties::create($data);
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['id'] =  $user->id;
        $success['role_type'] =  $user->role_type;
        $success['name'] =  $user->name;
        $success['email'] =  $user->email;

        return response()->json(['statusCode' => '200','data' =>$success ,"message" =>"User Register Successfully....!"]);
    }

    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['statusCode' => '400','data' => $validator->errors() ,"message" =>"Somthing Wrong Please try again"]);
        }
        if(Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')]))
        {
            $user = Auth::user();
            if($user->status == "inactive")
            {
                return response()->json(['statusCode'=> '400', "data"=> null,"message" => "Your Document Not Varify By Admin "]);
            }
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['data'] =  $user;
            $message =  "Login successfuly";
            return response()->json(['statusCode' => '200','data' =>  $success,'message' => $message]);    
        }
        else{
            return response()->json(['statusCode'=> '400', "data"=> null,"message" => "Email or Password not match"]);
        }
    }


    public function loginGoogle(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'id' => 'required',
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['statusCode' => '400','data' => $validator->errors() ,"message" =>"Somthing Wrong Please try again"]);
        }
        $data = $request->all();
        $user = User::where("email", $data['email'])->get()->first();
        if($user) { 
            Auth::login($user);
            //Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['data'] =  $user;
            $message =  "Login successfuly";
            return response()->json(['statusCode' => '200','data' =>  $success,'message' => $message]);
        }
        else{
            return response()->json(['statusCode'=> '400', "data"=> null,"message" => "Google Account not linked"]);
        }
    }

    public function edit_profile(Request $request)
    {
        $validator = Validator::make($request->all(), [  
            'location' => 'required',
            'email' => 'required',
            'mobileno' => 'required',  
            'designation' => 'required', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['statusCode' => '400','data' => $validator->errors() ,"message" =>"Somthing Wrong Please try again"]);
        }
            $data = $request->all();
            $user = Auth::user();
            $user->location=$request->input('location');
            $user->email=$request->input('email');
            $user->mobileno=$request->input('mobileno');
            $user->designation=$request->input('designation');
            $user->image=$request->input('image');
            $user->save(); 
            // if ($request->file('image')==null){
            //     $user->save();        
            // }
            // else{
            //     $user->image = $request->file('image')->hashName();
            //     $request->file('image')
            //     ->store('image', ['disk' => 'public']); 
            //     $user->save();       
            // }
            //$success['url'] = Config::get('constants.image').$user->image;
            $success['name'] =  $user->name;
            $success['email'] =  $user->email;

            return response()->json(['statusCode' => '200','data' =>  $success,'message' => "Profile has been Change successfully."]);
    }
    public function change_password(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required',
            // "c_password"=> 'required|same:new_password',
        ]);

        if (!(Hash::check($request->get('current_password'), $user->password))) {
            return response()->json(['statusCode'=> '400', "data"=> null,"message" =>"Your current password does not matches with the password you provided. Please try again."]);
        }
        
        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            return response()->json(['statusCode'=> '400', "data"=> null,"message" => "New Password cannot be same as your current password. Please choose a different password."]);
        }
        
        $user->password = bcrypt($request->get('new_password'));
        $user->save();
        $success['username'] = $user->username;
        $success['email'] = $user->email;
        return response()->json(['statusCode' => '200','data' =>$success,'message' => "password has been Change successfully."]);
    }
    public function profile()
    {
        $user = Auth::user();
        $user_list = User::with('userExpertise.expert:id,name')
                            //->select('id','name','location','designation','interests','image')
                            ->where('id',$user->id)
                            ->get();
        return response()->json(['statusCode' => '200','data' =>  $user_list,'message' => "User Detail."]);

    }
    public function update_coverimage(Request $request)
    {
        $validator = Validator::make($request->all(), [    
            'cover_image' => 'required', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['statusCode' => '400','data' => $validator->errors() ,"message" =>"Somthing Wrong Please try again"]);
        }
            $data = $request->all();
            $user = Auth::user();
            if ($request->file('cover_image')!=null){
                $user->cover_image = $request->file('cover_image')->hashName();
                $request->file('cover_image')
                ->store('cover_image', ['disk' => 'public']); 
            }
            $user->save();      
            //$success['url'] = Config::get('constants.image').$user->image;
            $success['name'] =  $user->name;
            $success['email'] =  $user->email;
            $success['cover_image'] =  $user->cover_image;

            return response()->json(['statusCode' => '200','data' =>  $success,'message' => "Cover Image Update successfully."]);
    }
    public function edit_experties(Request $request)
    {
        // $validator = Validator::make($request->all(), [  
        //     'location' => 'required',
        //     'email' => 'required',
        //     'mobileno' => 'required',  
        //     'designation' => 'required', 
        //     'about_me' => 'required', 
        //     'image' => 'required', 
        //     'cover_image' => 'required', 
        // ]);
        // if ($validator->fails()) { 
        //     return response()->json(['statusCode' => '400','data' => $validator->errors() ,"message" =>"Somthing Wrong Please try again"]);
        // }
        $data = $request->all();
        $user = Auth::user();
        if($user)
        {
            $user->username=$request->input('username');
            $user->location=$request->input('location');
            $user->mobileno=$request->input('mobileno');
            $user->designation=$request->input('designation');
            $user->image=$request->input('image');
            $user->cover_image=$request->input('cover_image');
            // $user->about_me=$request->input('about_me');
            // $user->interests=$request->input('interests');
            $user->save(); 
            //$success['cover_image'] = Config::get('constants.cover_image').$user->cover_image;
        }
        return response()->json(['statusCode' => '200','data' =>  $user,'message' => "Profile has been Change successfully."]);
    }
}
