<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Expert;
use App\User;
use App\Rating;
use App\UserExperties;
use Illuminate\Support\Facades\Auth; 
use App\Post;
use App\PostComment;
use App\PostLike;
use App\PostBookmark;
use App\PostExperties;

class ExpertController extends Controller
{
    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['statusCode' => '400','data' => $validator->errors() ,"message" =>"Somthing Wrong Please try again"]);
        }
        $data = $request->all();
        $expert_add = Expert::create($data);

        return response()->json(['statusCode' => '200','data' =>$expert_add ,"message" =>"Experts add Successfully....!"]);
    }
    public function expert_list()
    {
        $list = Expert::get();
        if(count($list) > 0){
            return response()->json(['statusCode' => '200','data' =>$list ,"message" =>"Experts Listing....!"]);
        }
        else{
            return response()->json(['statusCode' => '402','data' =>NULL ,"message" =>"No Record Found....!"]);
        }

    }
    public function experies_list(Request $request)
    {
        // $search = $request->get('name');
        // $where[] = ['users.name','like','%'.$search.'%'];
        // $where[] = ['users.role_id','3'];
        // $post_list = User::where($where)
        // ->get();
        // foreach($post_list as &$user) {
        //     $rating = Rating::with('user')->where('experties_id',$user->id)->get();
        //     $expert = UserExperties::with('expert')->where('user_id',$user->id)->get();
        //     $user['rating'] = $rating->avg('rating');
        //     $user['expertise'] = $expert;
        // }
        $post_list = User::with('userExpertise.expert:id,name')
                    ->select('id','name','username','location','designation','interests','image')
                    ->where('role_id','3')
                    ->get();
        if(count($post_list))
        {
            return response()->json(['statusCode' => '200','data' =>$post_list ,"message" =>"Experties Listing....!"]);
        }
        else{
            return response()->json(['statusCode' => '400','data' =>null ,"message" =>"No record found....!"]);
        }

    }
    public function get_experies($id)
    {
        $list = User::with('userExpertise.expert:id,name')->find($id);
        $list->posts = $this->post_list($id);
        $list->answers = $this->answerPost($id);
        return response()->json(['statusCode' => '200','data' =>$list ,"message" =>"Experties Details....!"]);
    }
    public function edit_experties(Request $request)
    {

    }
    
    
    private function answerPost($userId)
    {
        $user = Auth::user();
        $where[] = ['user_id','=',  $userId];
        
        $comments = PostComment::where("user_id", $userId)->get();
        $postIds = [];
        foreach($comments as $comment){
            $postIds[] = $comment->post_id;
        }
        
        $post_list = Post::select('posts.expert as for_expert', 'posts.*')->with('expert','user')
        ->join('users','users.id','posts.user_id')
        ->whereIn("posts.id", $postIds)
        ->orderBy("posts.id","desc")
        ->get();
        
        foreach($post_list as &$post){
            $post['answerCount'] = PostComment::where('post_id',$post->id)->count();
            $post['likeCount'] = PostLike::where('post_id',$post->id)->count();
            $post['bookmarkCount'] = PostBookmark::where('post_id',$post->id)->count();
            $post['is_like'] = (boolean) PostLike::where('post_id',$post->id)->where('user_id',$user->id)->count();
            $post['is_bookmark'] = (boolean) PostBookmark::where('post_id',$post->id)->where('user_id',$user->id)->count();
            if($post->for_expert == 1){
                 $post['canComment'] =  PostExperties::where("question_id", $post->id)->where("experties_id", $user->id)->limit(1)->count();
            }else{
                $post['canComment'] = 1;
            }
        }
        
        return $post_list;
    }
    
     private function post_list($userId)
    {
        $user = Auth::user();
        if($userId)
        {
            $where[] = ['posts.user_id','=',  $userId];
        }
        $post_list = Post::select('posts.expert as for_expert', 'posts.*')->with('expert','user')
        ->join('users','users.id','posts.user_id')
        ->where($where)
        ->orderBy("posts.id","desc")
        ->get();
        
        foreach($post_list as &$post){
            $post['answerCount'] = PostComment::where('post_id',$post->id)->count();
            $post['likeCount'] = PostLike::where('post_id',$post->id)->count();
            $post['bookmarkCount'] = PostBookmark::where('post_id',$post->id)->count();
            $post['is_like'] = (boolean) PostLike::where('post_id',$post->id)->where('user_id',$user->id)->count();
            $post['is_bookmark'] = (boolean) PostBookmark::where('post_id',$post->id)->where('user_id',$user->id)->count();
            if($post->for_expert == 1){
                 $post['canComment'] =  PostExperties::where("question_id", $post->id)->where("experties_id", $user->id)->limit(1)->count();
            }else{
                $post['canComment'] = 1;
            }
        }
        
        return $post_list;
    }

    
    
    
    
    
    
}
