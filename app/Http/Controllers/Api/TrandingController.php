<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Tranding;

class TrandingController extends Controller
{
    public function list()
    {
        $list = Tranding::get();
        if(count($list)>0)
        {
            return response()->json(['statusCode' => '200','data' => $list ,"message" =>"Trending List !......"]);
        }
        else{
            return response()->json(['statusCode' => '400','data' => null ,"message" =>"No data Found"]);

        }
    }
}
