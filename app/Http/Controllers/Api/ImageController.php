<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Config;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function image_upload(Request $request)
    {
        $data = $request->all();
        $data['image'] = $request->file('image');
        $dir = $data['dir'];
        if ($request->file('image')==null){        
        }
        else{
            $data['image'] = $request->file('image')->hashName();
            $request->file('image')
            ->store($dir, ['disk' => 'public']);
            $data['url'] = Config::get('constants.image').$dir.'/'.$data['image'];        
        }
        return response()->json(['statusCode' => '200','data' =>  $data,"message" =>"Image upload Successfully....!"]);
    }
}
