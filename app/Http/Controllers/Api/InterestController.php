<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Interest;

class InterestController extends Controller
{
    public function list()
    {
        $list = Interest::get();
        if(count($list)>0)
        {
            return response()->json(['statusCode' => '200','data' => $list ,"message" =>"Interest List !......"]);
        }
        else{
            return response()->json(['statusCode' => '400','data' => null ,"message" =>"No data Found"]);

        }
    }
}
