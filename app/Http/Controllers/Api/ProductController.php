<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Product;

class ProductController extends Controller
{
    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_sku' => 'required',
            'platform' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['statusCode' => '400','data' => $validator->errors() ,"message" =>"Somthing Wrong Please try again"]);
        }
        $data = $request->all();
        $add = Product::create($data);

        return response()->json(['statusCode' => '200','data' =>$add ,"message" =>"Product add sucessfully....!"]);
    }
    public function get(Request $request)
    {
        $platform = $request->get('platform');
        $where = [];
        $where[] = ['products.platform','like','%'.$platform.'%'];

        $get = Product::where($where)->get();
        if(count($get)>0)
        {
            return response()->json(['statusCode' => '200','data' =>$get ,"message" =>"Product Listing....!"]);
        }
        else{
            return response()->json(['statusCode' => '400','data' =>null ,"message" =>"No Data Found....!"]);
        }
    }
}
