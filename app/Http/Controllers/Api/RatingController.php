<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Validator;
use App\User;
use App\Rating;

class RatingController extends Controller
{
    public function make_rating(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'rating' => 'required',
            'expert_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['statusCode' => '400','data' => $validator->errors() ,"message" =>"Somthing Wrong Please try again"]);
        }
        $data = $request->all();
        $data['user_id'] = $user->id;
        $rating = Rating::create($data);
        return response()->json(['statusCode' => '200','data' =>$rating ,"message" =>"Rating add Successfully....!"]);
    }
    public function rating_count()
    {
        $user = Auth::user();
        $users = User::where('role_id','3')->get();
        foreach($users as &$user) {
            $expert = Rating::with('user')->where('expert_id',$user->id)->get();
            $user['rating'] = $expert->avg('rating');
        }
        return response()->json(['statusCode' => '200','data' =>$users ,"message" =>"experties list....!"]);

    }
}
