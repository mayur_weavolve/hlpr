<?php
Route::post('register', 'Api\UserController@register');
Route::post('login', 'Api\UserController@login');
Route::post('login-google', 'Api\UserController@loginGoogle');
Route::post('create', 'Api\PasswordResetController@create');
Route::get('find/{token}', 'Api\PasswordResetController@find');
Route::post('reset', 'Api\PasswordResetController@reset');

// Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
// Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::post('expert/add', 'Api\ExpertController@add');
Route::post('expert/list', 'Api\ExpertController@expert_list');
Route::post('experties/list', 'Api\ExpertController@experies_list');
Route::post('image/upload', 'Api\ImageController@image_upload');
Route::post('video/upload', 'Api\VideoController@video_upload');
Route::post('interest/list', 'Api\InterestController@list');
Route::post('trending/list', 'Api\TrandingController@list');

Route::group(['middleware' => 'auth:api'], function(){
    
    Route::post('experties/get/{id}', 'Api\ExpertController@get_experies');


    Route::post('profile', 'Api\UserController@profile');
    Route::post('profile/edit', 'Api\UserController@edit_profile');
    Route::post('profile/cover_image', 'Api\UserController@update_coverimage');
    Route::post('change_password', 'Api\UserController@change_password');
    Route::post('experties/profile/edit', 'Api\UserController@edit_experties');


    Route::post('post/add', 'Api\PostController@post_add');
    Route::post('post/delete/{id}', 'Api\PostController@post_delete');
    Route::post('post/edit/{id}', 'Api\PostController@post_edit');
    Route::post('post/list', 'Api\PostController@post_list');
    Route::post('post/like', 'Api\PostController@post_like');
    Route::post('post/like/list/{id}', 'Api\PostController@post_like_list');
    Route::post('post/comment', 'Api\PostController@post_comment');
    Route::post('post/comment/list/{id}', 'Api\PostController@post_comment_list');
    Route::post('post/answer', 'Api\PostController@post_answer');
    Route::post('post/detail/{id}', 'Api\PostController@post_detail');
    Route::post('notification/list', 'Api\PostController@notification');

    Route::post('post/bookmark', 'Api\PostController@post_bookmark');
    Route::post('post/bookmark/list', 'Api\PostController@post_bookmark_list');
    //Route::post('post/bookmark/delete/{id}', 'Api\PostController@bookmark_delete');

    Route::post('rating', 'Api\RatingController@make_rating');
    Route::post('rating/count', 'Api\RatingController@rating_count');

    Route::post('bank/detail', 'Api\BankController@add_bank_detail');
    Route::post('bank/detail/get', 'Api\BankController@get_bank_detail');

    Route::post('contactus/add', 'Api\ContactUsController@add');

    Route::post('product/add', 'Api\ProductController@add');
    Route::post('product/get', 'Api\ProductController@get');

    Route::post('order/add', 'Api\ProductPurchaseController@add');

    Route::post('payment/add', 'Api\ExpertPaymentController@add_payment');

});


?>